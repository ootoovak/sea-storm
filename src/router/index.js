import Vue from 'vue'
import Router from 'vue-router'
import Blockstack from '@/components/Blockstack'
import Hello from '@/components/Hello'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Blockstack',
      component: Blockstack
    }, {
      path: '/hello',
      name: 'Hello',
      component: Hello
    }
  ]
})
